// Every project should have this below in gitbash:
//npm init
//npm install express
//touch .gitignore

//npm install mongoose

// Setting up basic server
const express  = require("express");

// Mongoose is a package that allows creation of Schemas to model our data structure.
// Also has access to a number of methods for manipulating our database.
const mongoose = require("mongoose");

// Setup the express server
const app = express();

// List the port where the server will listen
const port = 3001;

// MongoDB Connection
// Connect to the database by passing in your connection string.
/*
	Syntax:
		mongoose.connect("<MongoDB Atlas Connection String>",
		{userNewUrlParser: true, UseUnifiedTopology: true});
*/

mongoose.connect("mongodb+srv://admin:admin@myfirstcluster.ni5hwok.mongodb.net/b183_to-do?retryWrites=true&w=majority", {useNewUrlParser: true, UseUnifiedTopology: true});

// Set a notification for connection success or error.
let db = mongoose.connection;

// if a connection error occurred, it will be output in the console.
// console.error.bind(console) allows us to print the error in the browser consoles and in the terminal.
db.on("error", console.error.bind(console, "connection error"))

// If the connection is successful, a console message will be shown.
db.once("open", () => console.log("We're connected to the cloud database."))

// Mongoose Schema
// Determine the structure of the document to be written in the database.
// Schemas act as blueprints to our data.
const taskSchema = new mongoose.Schema({
	// Name of the task
	name: String, // name: String is a shorthand for name:{type: String}
	// Status task (Completed, Pending, Incomplete)
	status: {
		type: String,
		// Default values are the predefined values for a field if we don't put any value.
		default: "Pending"
	}
	
})

// Mongoose Model
// Model uses Schemas and they act as the middleman from the server (JS code) to our database.
/*
	Syntax:
		const modelName = mongoose.model ("collectionName", mongooseSchema);
*/
	//Note: Model must be in singular form and capitalize the first letter.
	// Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into a plural form when creating a collection.
	const Task = mongoose.model("Task", taskSchema);

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Route for creating a task

// Business Logic
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task already exists in the database, we return an error
		- If the task doesn't exist in the database, we add it in the database
	2. The task data will be coming from the request's body
	3. Create a new Task object with a "name" field/property
	4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object

	Task is duplicated if:
	- result from the query not equal to null.
	- req.body.name is equal to result.name.
*/

app.post("/tasks", (req, res)=>{
	// Call Back Functions in mongoose methods are programmed this way:
		// first parameter store the error
		// second parameter return the result.
		// req.body.name = eat
		// Task.findOne ({name:eat})
	Task.findOne({name:req.body.name}, (err, result)=> {
		console.log(result);

		// if a document was found and the document's name matches the information sent via client/postman
		if (result != null && req.body.name == result.name){
			// Return a message to the client/postman
			return res.send("Duplicate task found!");
		}
		// if no document was found or no duplicate.
		else{
			// Create a new task and save to database

			let newTask = new Task({
				name: req.body.name
			});

			// The ".save" method will store the information to the database
			// Since the "newTask" was created/instantiated from the Task model that contains the Mongoose Schema, so it will gain access to the save method.

			newTask.save((saveErr, saveTask)=>{
				// If there are errors in saving it will be diplayed in the console.
				if(saveErr){
					return console.error(saveErr);
				}
				// If no error found while creating the document it will be save in the database.
				else{
					return res.status(201).send("New task created.");
				}
			})
		}
	})

})

// Business Logic
		/*
		1. Retrieve all the documents
		2. If an error is encountered, print the error
		3. If no errors are found, send a success status back to the client/Postman and return an array of documents
		*/

app.get("/tasks", (req,res)=> {
	Task.find({}, (err, result)=>{
		if(err){
			return console.log(err);
		}
		else{
			return res.status(200).send(result);
		}
	})
})

// Activity
/*

	Instructions s30 Activity:
	1. Create a User schema.
	2. Create a User model.
	3. Create a POST route that will access the "/signup" route that will create a user.
	4. Process a POST request at the "/signup" route using postman to register a user.
	5. Create a git repository named S30.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	7. Add the link in Boodle.

*/

const userSchema = new mongoose.Schema({	
	userName: String,
	password: {
		type: String,
	}
		
	
})
const User = mongoose.model("User", userSchema);

app.post("/signup", (req, res)=>{
	User.findOne({name: req.body.Username}, (err, result)=> {
		console.log(result);
		if (result != null && req.body.userName == result.userName){
			return res.send("User already exists");
		}
		else{
			if (req.body.username != "" && req.body.password != ""){
			let newUser = new User({
				userName: req.body.userName,
				password: req.body.password
			});
			
			newUser.save((saveErr, saveTask)=>{
				if(saveErr){
					return console.error(saveErr);
				}
				else{
					return res.status(201).send("New user created.");
				}
			})
		}
		else{
			return res.send("Please input username and password.");
		}
				
		}
		
	})

})

//Register a user

/*
	Business Logic:

	1. Add a functionality to check if there are duplicate tasks
		- If the user already exists in the database, we return an error
		- If the user doesn't exist in the database, we add it in the database
	2. The user data will be coming from the request's body
		- Note: Make sure that the username and password from the request is not empty.
	3. Create a new User object with a "username" and "password" fields/properties
*/


// Listen to the port
app.listen(port, () => console.log(`Server running at port ${port}`));


